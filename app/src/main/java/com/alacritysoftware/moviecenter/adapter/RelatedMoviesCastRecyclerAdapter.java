package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.RelatedMovieCastResponse.Cast;
import com.alacritysoftware.moviecenter.model.RelatedMovieCastResponse.RelatedMovieCastResponse;
import com.alacritysoftware.moviecenter.model.SimilarMovieResponseResults;
import com.bumptech.glide.Glide;

import java.util.List;

public class RelatedMoviesCastRecyclerAdapter extends RecyclerView.Adapter<RelatedMoviesCastRecyclerAdapter.RelatedMoviesCastHolder> {
    private List<Cast> relatedMovieCastResponseList;
    private RelatedMoviesCastRecyclerAdapterListener mRelatedMoviesCastRecyclerAdapterListener;

    public RelatedMoviesCastRecyclerAdapter(List<Cast> relatedMovieCastResponseList, RelatedMoviesCastRecyclerAdapterListener relatedMoviesCastRecyclerAdapterListener) {
        this.relatedMovieCastResponseList = relatedMovieCastResponseList;
        this.mRelatedMoviesCastRecyclerAdapterListener = relatedMoviesCastRecyclerAdapterListener;
    }

    public interface RelatedMoviesCastRecyclerAdapterListener {
        void detailcastMovieFromInside(int movie_id);
    }
    @NonNull
    @Override
    public RelatedMoviesCastHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movie_horizontal, parent, false);
        return new RelatedMoviesCastHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RelatedMoviesCastHolder holder, int position) {
        Cast mRelatedMovieCastResponseList = relatedMovieCastResponseList.get(position);
        holder.text_vote_average.setText(String.valueOf(mRelatedMovieCastResponseList.getVoteAverage()));
        holder.image_movie.setClipToOutline(true);
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+mRelatedMovieCastResponseList.getPosterPath())
                .into(holder.image_movie);
        holder.linearLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRelatedMoviesCastRecyclerAdapterListener.detailcastMovieFromInside(mRelatedMovieCastResponseList.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return relatedMovieCastResponseList.size();
    }

    class RelatedMoviesCastHolder extends RecyclerView.ViewHolder{
        TextView text_vote_average;
        ImageView image_movie;
        LinearLayout linearLayoutID;
        public RelatedMoviesCastHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_vote_average = itemView.findViewById(R.id.text_vote_average);
            image_movie = itemView.findViewById(R.id.image_movie);
        }
    }
}
