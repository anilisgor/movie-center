package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.PopularMovieResponseResults;
import com.bumptech.glide.Glide;

import java.util.List;

public class MovieListRecyclerAdapter extends RecyclerView.Adapter<MovieListRecyclerAdapter.MovieHolder> {

    private List<PopularMovieResponseResults> popularMovieResponseResults;
    private MovieListRecyclerAdapterListener mMovieListRecyclerAdapterListener;

    public MovieListRecyclerAdapter(List<PopularMovieResponseResults> popularMovieResponseResults, MovieListRecyclerAdapterListener movieListRecyclerAdapterListener) {
        this.popularMovieResponseResults = popularMovieResponseResults;
        this.mMovieListRecyclerAdapterListener = movieListRecyclerAdapterListener;
    }
    public interface MovieListRecyclerAdapterListener {
        void favoriteMovie(PopularMovieResponseResults populerResponse);
        void detailMovie(int movie_id);
    }
    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movie_horizontal, parent, false);

        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        PopularMovieResponseResults mPopularMovieResponseResults = popularMovieResponseResults.get(position);
       // holder.text_header.setText(mPopulerMovieResponseResults.getTitle());
        //holder.text_explanation.setText(mPopulerMovieResponseResults.getOverview());
        holder.image_movie.setClipToOutline(true);
        holder.text_vote_average.setText(String.valueOf(mPopularMovieResponseResults.getVote_average()));
        /*if(mPopulerMovieResponseResults.isFavorite()){
            holder.image_favbutton.setImageResource(R.drawable.ic_star_black_24dp);
        }
        else {
            holder.image_favbutton.setImageResource(R.drawable.ic_star_border_black_24dp);
        }*/
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+ mPopularMovieResponseResults.getPoster_path())
                .into(holder.image_movie);
        /*holder.image_favbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopulerMovieResponseResults.setFavorite(!mPopulerMovieResponseResults.isFavorite());
                mMovieListRecyclerAdapterListener.favoriteMovie(mPopulerMovieResponseResults);
                notifyDataSetChanged();

            }
        });*/

        holder.linearLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMovieListRecyclerAdapterListener.detailMovie(mPopularMovieResponseResults.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return popularMovieResponseResults.size();
    }



    class MovieHolder extends RecyclerView.ViewHolder {
        TextView text_header,text_vote_average/*, text_explanation*/;
        ImageView image_movie;
        //ImageButton image_favbutton;
        LinearLayout linearLayoutID;
        public MovieHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_vote_average = itemView.findViewById(R.id.text_vote_average);
            //text_header = itemView.findViewById(R.id.text_header);
            //text_explanation = itemView.findViewById(R.id.text_explanation);
            image_movie = itemView.findViewById(R.id.image_movie);
            //image_favbutton = itemView.findViewById(R.id.image_favbutton);
        }
    }
}
