package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.CastsMovieResponseCast;
import com.bumptech.glide.Glide;

import java.util.List;

public class CastMovieRecyclerAdapter extends RecyclerView.Adapter<CastMovieRecyclerAdapter.CastHolder> {
    private CastMovieRecyclerAdapterListener castMovieRecyclerAdapterListener;
    private List<CastsMovieResponseCast> castsMovieResponseCasts;

    public CastMovieRecyclerAdapter(List<CastsMovieResponseCast> castsMovieResponseCasts, CastMovieRecyclerAdapterListener castMovieRecyclerAdapterListener) {
        this.castMovieRecyclerAdapterListener = castMovieRecyclerAdapterListener;
        this.castsMovieResponseCasts = castsMovieResponseCasts;
    }
    public interface CastMovieRecyclerAdapterListener {
        void detailPerson(int person_id);
    }

    @NonNull
    @Override
    public CastHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_cast, parent, false);
        return new CastMovieRecyclerAdapter.CastHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastHolder holder, int position) {
        CastsMovieResponseCast mCastsMovieResponseCast = castsMovieResponseCasts.get(position);
        holder.text_character.setText(mCastsMovieResponseCast.getCharacter());
        holder.text_originalname.setText(mCastsMovieResponseCast.getOriginal_name());
        //holder.text_popularity.setText(" / "+String.valueOf(mCastsMovieResponseCast.getPopularity()));
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+mCastsMovieResponseCast.getProfile_path()).circleCrop()
                .error(R.drawable.no_profile_photo)
                .into(holder.image_cast);
        holder.linearlayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), String.valueOf(mCastsMovieResponseCast.getİd()), Toast.LENGTH_SHORT).show();
                castMovieRecyclerAdapterListener.detailPerson(mCastsMovieResponseCast.getİd());
            }
        });
    }

    @Override
    public int getItemCount() {
        return castsMovieResponseCasts.size();
    }



    class CastHolder extends RecyclerView.ViewHolder {
        ImageView image_cast;
        TextView text_originalname, text_character, text_popularity;
        LinearLayout linearlayoutID;

        public CastHolder(@NonNull View itemView) {
            super(itemView);
            //text_popularity = itemView.findViewById(R.id.text_popularity);
            text_character = itemView.findViewById(R.id.text_character);
            linearlayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_originalname = itemView.findViewById(R.id.text_originalname);
            image_cast = itemView.findViewById(R.id.image_cast);
        }
    }
}
