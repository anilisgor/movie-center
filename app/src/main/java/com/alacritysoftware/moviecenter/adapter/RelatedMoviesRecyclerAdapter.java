package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.SimilarMovieResponseResults;
import com.bumptech.glide.Glide;

import java.util.List;

public class RelatedMoviesRecyclerAdapter extends RecyclerView.Adapter<RelatedMoviesRecyclerAdapter.RelatedMoviesHolder> {
    private List<SimilarMovieResponseResults> similarMovieResponseResults;
    private RelatedMoviesRecyclerAdapterListener mRelatedMoviesRecyclerAdapterListener;

    public RelatedMoviesRecyclerAdapter(List<SimilarMovieResponseResults> similarMovieResponseResults, RelatedMoviesRecyclerAdapterListener relatedMoviesRecyclerAdapterListener) {
        this.similarMovieResponseResults = similarMovieResponseResults;
        this.mRelatedMoviesRecyclerAdapterListener = relatedMoviesRecyclerAdapterListener;
    }
    public interface RelatedMoviesRecyclerAdapterListener {
        void detailMovieFromInside(int movie_id);
    }
    @NonNull
    @Override
    public RelatedMoviesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movie_horizontal, parent, false);
        return new RelatedMoviesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RelatedMoviesHolder holder, int position) {
        SimilarMovieResponseResults mSimilarMovieResponseResults = similarMovieResponseResults.get(position);
        holder.text_vote_average.setText(String.valueOf(mSimilarMovieResponseResults.getVote_average()));
        holder.image_movie.setClipToOutline(true);
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+mSimilarMovieResponseResults.getPoster_path())
                .into(holder.image_movie);
        holder.linearLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRelatedMoviesRecyclerAdapterListener.detailMovieFromInside(mSimilarMovieResponseResults.getİd());
            }
        });
    }

    @Override
    public int getItemCount() {
        return similarMovieResponseResults.size();
    }

    class RelatedMoviesHolder extends RecyclerView.ViewHolder{
        TextView text_vote_average;
        ImageView image_movie;
        LinearLayout linearLayoutID;
        public RelatedMoviesHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_vote_average = itemView.findViewById(R.id.text_vote_average);
            image_movie = itemView.findViewById(R.id.image_movie);
        }
    }
}
