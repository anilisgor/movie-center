package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.SearchMovieResponseResults;
import com.bumptech.glide.Glide;

import java.util.List;

public class SearchMovieRecyclerAdapter extends RecyclerView.Adapter<SearchMovieRecyclerAdapter.SearchHolder> {

    private List<SearchMovieResponseResults> searchMovieResponseResults;
    private SearchMovieRecyclerAdapterListener searchMovieRecyclerAdapterListener;

    public SearchMovieRecyclerAdapter(List<SearchMovieResponseResults> searchMovieResponseResults, SearchMovieRecyclerAdapterListener searchMovieRecyclerAdapterListener) {
        this.searchMovieResponseResults = searchMovieResponseResults;
        this.searchMovieRecyclerAdapterListener = searchMovieRecyclerAdapterListener;
    }
    public interface SearchMovieRecyclerAdapterListener{
        void searchMovieResult(SearchMovieResponseResults populerResponse);
        void detailMovieFromSearch(int movie_id);
    }
    @NonNull
    @Override
    public SearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movies, parent, false);
        return new SearchHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchHolder holder, int position) {
        SearchMovieResponseResults mSearchMovieResponseResults = searchMovieResponseResults.get(position);
        holder.text_header.setText(mSearchMovieResponseResults.getTitle());
        holder.text_explanation.setText(mSearchMovieResponseResults.getOverview());
        if(mSearchMovieResponseResults.isFavorite()){
            holder.image_favbutton.setImageResource(R.drawable.ic_star_black_24dp);
        }
        else {
            holder.image_favbutton.setImageResource(R.drawable.ic_star_border_black_24dp);
        }
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+mSearchMovieResponseResults.getPoster_path())
                .into(holder.image_movie);
        // holder.text_header.setText(mPopulerMovieResponseResults.getTitle());
        holder.image_favbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchMovieResponseResults.setFavorite(!mSearchMovieResponseResults.isFavorite());
                searchMovieRecyclerAdapterListener.searchMovieResult(mSearchMovieResponseResults);
                notifyDataSetChanged();

            }
        });
        holder.linearLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchMovieRecyclerAdapterListener.detailMovieFromSearch(mSearchMovieResponseResults.getİd());
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchMovieResponseResults.size();
    }

    class SearchHolder extends RecyclerView.ViewHolder {
        TextView text_header, text_explanation;
        ImageView image_movie;
        ImageButton image_favbutton;
        LinearLayout linearLayoutID;
        public SearchHolder(@NonNull View itemView) {
            super(itemView);
            text_header = itemView.findViewById(R.id.text_header);
            text_explanation = itemView.findViewById(R.id.text_explanation);
            image_movie = itemView.findViewById(R.id.image_movie);
            image_favbutton = itemView.findViewById(R.id.image_favbutton);
            linearLayoutID = itemView.findViewById(R.id.linearlayoutID);
        }
    }
}
