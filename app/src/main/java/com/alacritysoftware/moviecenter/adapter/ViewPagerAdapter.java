package com.alacritysoftware.moviecenter.adapter;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.alacritysoftware.moviecenter.ui.fragments.moviedetail.about.InsideDetailAboutFragment;
import com.alacritysoftware.moviecenter.ui.fragments.moviedetail.casts.InsideDetailCastFragment;
import com.alacritysoftware.moviecenter.ui.fragments.moviedetail.relatedmovies.InsideDetailRelatedMoviesFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter implements InsideDetailRelatedMoviesFragment.InsideDetailRelatedMoviesFragmentEventListener,
        InsideDetailCastFragment.InsideDetailCastFragmentEventListener {
    int mNoOfTabs,movie_id;
    public  ViewPagerAdapterEventListener viewPagerAdapterEventListener;
    public ViewPagerAdapter(FragmentManager fm, int NumberOfTabs, int movieId, ViewPagerAdapterEventListener viewPagerAdapterEventListener ) {
        super(fm);
        this.mNoOfTabs =  NumberOfTabs;
        this.movie_id =  movieId;
        this.viewPagerAdapterEventListener = viewPagerAdapterEventListener;
    }



    public interface ViewPagerAdapterEventListener {
        void onDetailMovieFromInsideClick(int movie_id);
        void onDetailPersonFromClick(int person_id);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new InsideDetailAboutFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("movie_id", movie_id);
            fragment.setArguments(bundle);

        }
        else if (position == 1)
        {
            fragment = new InsideDetailCastFragment().newInstance(this);
            Bundle bundle = new Bundle();
            bundle.putInt("movie_id", movie_id);
            fragment.setArguments(bundle);
        }
        else if (position == 2)
        {
            fragment = new InsideDetailRelatedMoviesFragment().newInstance(this);
            Bundle bundle = new Bundle();
            bundle.putInt("movieID", movie_id);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }

    @Override
    public void detailMovieShowInside(int movie_id) {
        viewPagerAdapterEventListener.onDetailMovieFromInsideClick(movie_id);
    }
    @Override
    public void detailPersonSend(int person_id) {
        viewPagerAdapterEventListener.onDetailPersonFromClick(person_id);
    }

   /* @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Tab-1";
        }
        else if (position == 1)
        {
            title = "Tab-2";
        }
       *//* else if (position == 2)
        {
            title = "Tab-3";
        }*//*
        return title;
    }*/
}