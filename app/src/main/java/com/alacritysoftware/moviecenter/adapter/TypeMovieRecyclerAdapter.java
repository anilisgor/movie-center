package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.DetailMoviesResponseGenres;

import java.util.List;

public class TypeMovieRecyclerAdapter extends RecyclerView.Adapter<TypeMovieRecyclerAdapter.TypeMovieHolder> {
    private List<DetailMoviesResponseGenres> detailMoviesResponseGenres;
    private TypeMovieRecyclerAdapterListener typeMovieRecyclerAdapterListener;

    public TypeMovieRecyclerAdapter(List<DetailMoviesResponseGenres> detailMoviesResponseGenres, TypeMovieRecyclerAdapterListener typeMovieRecyclerAdapterListener) {
        this.detailMoviesResponseGenres = detailMoviesResponseGenres;
        this.typeMovieRecyclerAdapterListener = typeMovieRecyclerAdapterListener;
    }

    @NonNull
    @Override
    public TypeMovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_types, parent, false);

        return new TypeMovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TypeMovieHolder holder, int position) {
        DetailMoviesResponseGenres mDetailMoviesResponseGenres = detailMoviesResponseGenres.get(position);
        holder.text_type.setText(mDetailMoviesResponseGenres.getName());
    }

    @Override
    public int getItemCount() {
        return detailMoviesResponseGenres.size();
    }

    public interface TypeMovieRecyclerAdapterListener {
    }

    class TypeMovieHolder extends RecyclerView.ViewHolder {
        TextView text_type;

        public TypeMovieHolder(@NonNull View itemView) {
            super(itemView);
            text_type = itemView.findViewById(R.id.text_type);
        }
    }
}
