package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.UpcomingResponseResults;
import com.bumptech.glide.Glide;

import java.util.List;

public class UpcomingMovieRecyclerAdapter extends RecyclerView.Adapter<UpcomingMovieRecyclerAdapter.UpcomingHolder>{
    private List<UpcomingResponseResults> upcomingResponseResultsList;
    private UpcomingMovieRecyclerAdapterListener mUpcomingMovieRecyclerAdapterListener;
    public UpcomingMovieRecyclerAdapter(List<UpcomingResponseResults> upcomingResponseResultsList, UpcomingMovieRecyclerAdapterListener upcomingMovieRecyclerAdapterListener) {
        this.upcomingResponseResultsList = upcomingResponseResultsList;
        this.mUpcomingMovieRecyclerAdapterListener = upcomingMovieRecyclerAdapterListener;
    }
    public interface UpcomingMovieRecyclerAdapterListener {
        void detailMovie(int movie_id);
    }

    @NonNull
    @Override
    public UpcomingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movie_horizontal, parent, false);

        return new UpcomingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UpcomingHolder holder, int position) {
        UpcomingResponseResults mUpcomingResponseResults = upcomingResponseResultsList.get(position);
        holder.text_vote_average.setText(String.valueOf(mUpcomingResponseResults.getVote_average()));
        holder.image_movie.setClipToOutline(true);
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+mUpcomingResponseResults.getPoster_path())
                .error(R.drawable.no_image_avaliable)
                .into(holder.image_movie);

        holder.linearLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUpcomingMovieRecyclerAdapterListener.detailMovie(mUpcomingResponseResults.getİd());
            }
        });
    }

    @Override
    public int getItemCount() {
        return upcomingResponseResultsList.size();
    }

    class UpcomingHolder extends RecyclerView.ViewHolder{
        TextView text_vote_average;
        ImageView image_movie;
        LinearLayout linearLayoutID;
        public UpcomingHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_vote_average = itemView.findViewById(R.id.text_vote_average);
            image_movie = itemView.findViewById(R.id.image_movie);
        }
    }
}
