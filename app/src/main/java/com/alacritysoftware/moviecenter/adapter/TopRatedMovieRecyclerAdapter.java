package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.TopRatedMovieResponseResults;
import com.bumptech.glide.Glide;

import java.util.List;

public class TopRatedMovieRecyclerAdapter extends RecyclerView.Adapter<TopRatedMovieRecyclerAdapter.TopRatedHolder> {

    private List<TopRatedMovieResponseResults> topRatedMovieResponseResults;
    private TopRatedMovieRecyclerAdapterListener mTopRatedMovieRecyclerAdapterListener;

    public TopRatedMovieRecyclerAdapter(List<TopRatedMovieResponseResults> topRatedMovieResponseResults, TopRatedMovieRecyclerAdapterListener topRatedMovieRecyclerAdapterListener) {
        this.topRatedMovieResponseResults = topRatedMovieResponseResults;
        this.mTopRatedMovieRecyclerAdapterListener = topRatedMovieRecyclerAdapterListener;
    }
    public interface TopRatedMovieRecyclerAdapterListener {
        void detailMovie(int movie_id);
    }
    @NonNull
    @Override
    public TopRatedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movie_horizontal, parent, false);

        return new TopRatedHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopRatedMovieRecyclerAdapter.TopRatedHolder holder, int position) {
        TopRatedMovieResponseResults mTopRatedMovieResponseResults = topRatedMovieResponseResults.get(position);
        holder.text_vote_average.setText(String.valueOf(mTopRatedMovieResponseResults.getVote_average()));
        holder.image_movie.setClipToOutline(true);
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+mTopRatedMovieResponseResults.getPoster_path())
                .error(R.drawable.no_image_avaliable)
                .into(holder.image_movie);

        holder.linearLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTopRatedMovieRecyclerAdapterListener.detailMovie(mTopRatedMovieResponseResults.getİd());
            }
        });
    }

    @Override
    public int getItemCount() {
        return topRatedMovieResponseResults.size();
    }

    class TopRatedHolder extends RecyclerView.ViewHolder {
        TextView text_vote_average;
        ImageView image_movie;
        LinearLayout linearLayoutID;


        public TopRatedHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_vote_average = itemView.findViewById(R.id.text_vote_average);
            image_movie = itemView.findViewById(R.id.image_movie);
        }
    }


}
