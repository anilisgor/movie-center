package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.CastsMovieResponseCast;
import com.alacritysoftware.moviecenter.model.PersonPopularResponseResults;
import com.bumptech.glide.Glide;

import java.util.List;

public class CastPopularPeopleRecyclerAdapter extends RecyclerView.Adapter<CastPopularPeopleRecyclerAdapter.CastPopularPeopleHolder>  {

    private CastPopularPeopleRecyclerAdapterListener castPopularPeopleRecyclerAdapterListener;
    private List<PersonPopularResponseResults> personPopularResponseResults;

    public CastPopularPeopleRecyclerAdapter(List<PersonPopularResponseResults> personPopularResponseResults, CastPopularPeopleRecyclerAdapterListener castPopularPeopleRecyclerAdapterListener) {
        this.castPopularPeopleRecyclerAdapterListener = castPopularPeopleRecyclerAdapterListener;
        this.personPopularResponseResults = personPopularResponseResults;
    }
    public interface CastPopularPeopleRecyclerAdapterListener {
        void detailPerson(int person_id);
    }

    @NonNull
    @Override
    public CastPopularPeopleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_cast_horizontal, parent, false);
        return new CastPopularPeopleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastPopularPeopleHolder holder, int position) {
        PersonPopularResponseResults mPersonPopularResponseResults = personPopularResponseResults.get(position);
        holder.text_originalname.setText(mPersonPopularResponseResults.getName());
        //holder.text_popularity.setText(" / "+String.valueOf(mCastsMovieResponseCast.getPopularity()));
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+mPersonPopularResponseResults.getProfile_path()).circleCrop()
                .error(R.drawable.no_profile_photo)
                .into(holder.image_cast);
        holder.linearlayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                castPopularPeopleRecyclerAdapterListener.detailPerson(mPersonPopularResponseResults.getİd());
            }
        });
    }



    @Override
    public int getItemCount() {
        return personPopularResponseResults.size();
    }



    class CastPopularPeopleHolder extends RecyclerView.ViewHolder {
        ImageView image_cast;
        TextView text_originalname, text_character;
        LinearLayout linearlayoutID;

        public CastPopularPeopleHolder(@NonNull View itemView) {
            super(itemView);
            //text_popularity = itemView.findViewById(R.id.text_popularity);
            text_character = itemView.findViewById(R.id.text_character);
            linearlayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_originalname = itemView.findViewById(R.id.text_originalname);
            image_cast = itemView.findViewById(R.id.image_cast);
        }
    }
}
