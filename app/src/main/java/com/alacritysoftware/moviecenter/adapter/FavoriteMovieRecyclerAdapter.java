package com.alacritysoftware.moviecenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.local.FavoriteMovie;
import com.alacritysoftware.moviecenter.R;
import com.bumptech.glide.Glide;

import java.util.List;

import io.realm.RealmResults;

public class FavoriteMovieRecyclerAdapter extends RecyclerView.Adapter<FavoriteMovieRecyclerAdapter.FavoriteHolder> {
    private List<FavoriteMovie> favoriteMoviesList;
    private FavoriteMovieRecyclerAdapterListener favoriteMovieRecyclerAdapterListener;



    public FavoriteMovieRecyclerAdapter(List<FavoriteMovie> favoriteMoviesList, FavoriteMovieRecyclerAdapterListener favoriteMovieRecyclerAdapterListener) {
        this.favoriteMovieRecyclerAdapterListener = favoriteMovieRecyclerAdapterListener;
        this.favoriteMoviesList = favoriteMoviesList;
    }

    @NonNull
    @Override
    public FavoriteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movies, parent, false);
        return new FavoriteHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteHolder holder, int position) {
        FavoriteMovie favoriteMovie = favoriteMoviesList.get(position);

        holder.text_header.setText(favoriteMovie.getMovieTitle());
        holder.text_explanation.setText(favoriteMovie.getMovieOverview());
        holder.image_favbutton.setImageResource(R.drawable.ic_star_black_24dp);
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/w500"+favoriteMovie.getMoviewPosterpath())
                .into(holder.image_movie);
        holder.image_favbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteMovieRecyclerAdapterListener.onFavoriteClick(favoriteMovie);
                favoriteMoviesList.remove(position);
                notifyDataSetChanged();

            }
        });
        holder.linearLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteMovieRecyclerAdapterListener.detailMovie(favoriteMovie.getMovieID());
            }
        });


    }

    @Override
    public int getItemCount() {
        return favoriteMoviesList.size();
    }

    public interface FavoriteMovieRecyclerAdapterListener {
        void onFavoriteClick(FavoriteMovie favoriteMovies);
        void detailMovie(int movie_id);
    }

    class FavoriteHolder extends RecyclerView.ViewHolder {
        TextView text_header, text_explanation;
        ImageView image_movie;
        ImageButton image_favbutton;
        LinearLayout linearLayoutID;

        public FavoriteHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutID = itemView.findViewById(R.id.linearlayoutID);
            text_header = itemView.findViewById(R.id.text_header);
            text_explanation = itemView.findViewById(R.id.text_explanation);
            image_movie = itemView.findViewById(R.id.image_movie);
            image_favbutton = itemView.findViewById(R.id.image_favbutton);
        }
    }
}
