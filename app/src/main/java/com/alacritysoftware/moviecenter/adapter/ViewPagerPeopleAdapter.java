package com.alacritysoftware.moviecenter.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.alacritysoftware.moviecenter.ui.fragments.moviecasts.about.InsidePersonDetailAboutFragment;
import com.alacritysoftware.moviecenter.ui.fragments.moviecasts.inmovies.InsidePersonDetailMoviesInFragment;

public class ViewPagerPeopleAdapter extends FragmentPagerAdapter implements InsidePersonDetailMoviesInFragment.InsidePersonDetailMoviesInFragmentEventListener {

    int mNoOfTabs,person_id;
    public ViewPagerPeopleAdapterListener viewPagerPeopleAdapterListener;
    public ViewPagerPeopleAdapter(@NonNull FragmentManager fm,int NumberOfTabs, int person_id, ViewPagerPeopleAdapterListener viewPagerPeopleAdapterListener) {
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
        this.person_id = person_id;
        this.viewPagerPeopleAdapterListener = viewPagerPeopleAdapterListener;
    }

    @Override
    public void detailCastMovieShowInside(int movie_id) {
        viewPagerPeopleAdapterListener.onDetailCastMovieFromInsideClick(movie_id);
    }


    public interface ViewPagerPeopleAdapterListener {
        void onDetailCastMovieFromInsideClick(int movie_id);
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new InsidePersonDetailAboutFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("person_id", person_id);
            fragment.setArguments(bundle);

        }
        else if (position == 1)
        {
            fragment = new InsidePersonDetailMoviesInFragment().newInstance(this);
            Bundle bundle = new Bundle();
            bundle.putInt("person_id", person_id);
            fragment.setArguments(bundle);
        }
        /*else if (position == 2)
        {

        }*/
        return fragment;
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}
