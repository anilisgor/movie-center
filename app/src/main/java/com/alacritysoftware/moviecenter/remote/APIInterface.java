package com.alacritysoftware.moviecenter.remote;

import com.alacritysoftware.moviecenter.model.CastsMovieResponse;
import com.alacritysoftware.moviecenter.model.DetailMoviesResponse;
import com.alacritysoftware.moviecenter.model.PeopleDetailResponse;
import com.alacritysoftware.moviecenter.model.PeopleImagesResponse;
import com.alacritysoftware.moviecenter.model.PersonPopularResponse;
import com.alacritysoftware.moviecenter.model.PopularMovieResponse;
import com.alacritysoftware.moviecenter.model.RelatedMovieCastResponse.RelatedMovieCastResponse;
import com.alacritysoftware.moviecenter.model.SearchMovieResponse;
import com.alacritysoftware.moviecenter.model.SimilarMovieResponse;
import com.alacritysoftware.moviecenter.model.TopRatedMovieResponse;
import com.alacritysoftware.moviecenter.model.UpcomingResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("movie/popular")
    Call<PopularMovieResponse> getPopulerMovies(@Query("api_key") String apiKey);

    @GET("movie/top_rated")
    Call<TopRatedMovieResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/upcoming")
    Call<UpcomingResponse> getUpcomingMovies(@Query("api_key") String apiKey);

    @GET("search/movie")
    Call<SearchMovieResponse> getSearchMovie(@Query("api_key") String apiKey, @Query("query") String query);

    @GET("movie/{movie_id}")
    Call<DetailMoviesResponse> getDetailMovie(@Path("movie_id") int movie_id, @Query("api_key") String apiKey );

    @GET("movie/{movie_id}/credits")
    Call<CastsMovieResponse> getCastMovie(@Path("movie_id") int movie_id, @Query("api_key") String apiKey );

    @GET("movie/{movie_id}/similar")
    Call<SimilarMovieResponse> getSimilarMovie(@Path("movie_id") int movie_id, @Query("api_key") String apiKey );

    @GET("person/{person_id}/movie_credits")
    Call<RelatedMovieCastResponse> getSimilarCastMovie(@Path("person_id") int person_id, @Query("api_key") String apiKey );

    @GET("person/popular")
    Call<PersonPopularResponse> getPopulerPeople(@Query("api_key") String apiKey);

    @GET("person/{person_id}")
    Call<PeopleDetailResponse> getPeopleDetail(@Path("person_id") int person_id, @Query("api_key") String apiKey );

    @GET("person/{person_id}/images")
    Call<PeopleImagesResponse> getPeopleImagesDetail(@Path("person_id") int person_id, @Query("api_key") String apiKey );
/*
    @POST("/api/users")
    Call<User> createUser(@Body User user);

    @GET("/api/users?")
    Call<UserList> doGetUserList(@Query("page") String page);

    @FormUrlEncoded
    @POST("/api/users?")
    Call<UserList> doCreateUserWithField(@Field("name") String name, @Field("job") String job);*/
}
