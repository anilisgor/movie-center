package com.alacritysoftware.moviecenter.ui.fragments.movielist;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.local.FavoriteMovie;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.CastPopularPeopleRecyclerAdapter;
import com.alacritysoftware.moviecenter.adapter.MovieListRecyclerAdapter;
import com.alacritysoftware.moviecenter.adapter.TopRatedMovieRecyclerAdapter;
import com.alacritysoftware.moviecenter.adapter.UpcomingMovieRecyclerAdapter;
import com.alacritysoftware.moviecenter.model.PersonPopularResponse;
import com.alacritysoftware.moviecenter.model.PersonPopularResponseResults;
import com.alacritysoftware.moviecenter.model.PopularMovieResponse;
import com.alacritysoftware.moviecenter.model.PopularMovieResponseResults;
import com.alacritysoftware.moviecenter.model.TopRatedMovieResponse;
import com.alacritysoftware.moviecenter.model.TopRatedMovieResponseResults;
import com.alacritysoftware.moviecenter.model.UpcomingResponse;
import com.alacritysoftware.moviecenter.model.UpcomingResponseResults;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MovieListFragment extends Fragment implements MovieListRecyclerAdapter.MovieListRecyclerAdapterListener,
        SwipeRefreshLayout.OnRefreshListener,
        TopRatedMovieRecyclerAdapter.TopRatedMovieRecyclerAdapterListener,
        UpcomingMovieRecyclerAdapter.UpcomingMovieRecyclerAdapterListener,
        CastPopularPeopleRecyclerAdapter.CastPopularPeopleRecyclerAdapterListener {
    private RecyclerView recyclerViewMovieList, recyclerViewTopRatedList, recyclerViewUpcomingList,recyclerViewPopulerPeopleList;
    private MovieListRecyclerAdapter movieListRecyclerAdapter;
    private TopRatedMovieRecyclerAdapter topRatedMovieRecyclerAdapter;
    private UpcomingMovieRecyclerAdapter upcomingMovieRecyclerAdapter;
    private CastPopularPeopleRecyclerAdapter castPopularPeopleRecyclerAdapter;
    private List<PopularMovieResponseResults> popularMovieResponseResultsList;
    private List<TopRatedMovieResponseResults> topRatedMovieResponseResultsList;
    private List<UpcomingResponseResults> upcomingResponseResultsList;
    private List<PersonPopularResponseResults> personPopulerResponseResultsList;
    private MovieListFragmentEventListener mMovieListFragmentEventListener;
    private APIInterface apiInterface;
    private Realm mRealm;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    public void onRefresh() {
        //getPopulerMoviesWithSwipe();

    }

    @Override
    public void detailPerson(int person_id) {
        mMovieListFragmentEventListener.detailPersonShow(person_id);
    }

    public interface MovieListFragmentEventListener {
        void detailMovieShow(int movie_id);
        void detailPersonShow(int person_id);
    }

    @Override
    public void detailMovie(int movie_id) {
        mMovieListFragmentEventListener.detailMovieShow(movie_id);
    }


    public MovieListFragment newInstance(MovieListFragmentEventListener movieListFragmentEventListener) {
        MovieListFragment fragment = new MovieListFragment();
        fragment.mMovieListFragmentEventListener = movieListFragmentEventListener;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
        // Inflate the layout for this fragment
        init(view);
        getTopRatedMovies();
        getPopulerMovies();
        getUpcomingMovies();
        getPopulerPeople();
        //swipeToRefresh();

        return view;
    }

    /*
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                getPopulerMoviesWithSwipe();
            }
        });
    }*/
    private void init(View view) {
        //mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        recyclerViewMovieList = view.findViewById(R.id.recyclerViewMovieList);
        recyclerViewTopRatedList = view.findViewById(R.id.recyclerViewTopRatedList);
        recyclerViewUpcomingList = view.findViewById(R.id.recyclerViewUpcomingList);
        recyclerViewPopulerPeopleList = view.findViewById(R.id.recyclerViewPopulerPeopleList);

        apiInterface = ApiClient.getClient().create(APIInterface.class);
        popularMovieResponseResultsList = new ArrayList<>();
        topRatedMovieResponseResultsList = new ArrayList<>();
        upcomingResponseResultsList = new ArrayList<>();
        personPopulerResponseResultsList = new ArrayList<>();
        mRealm = Realm.getDefaultInstance();
        recyclerViewMovieList.setNestedScrollingEnabled(false);
        recyclerViewTopRatedList.setNestedScrollingEnabled(false);
        recyclerViewUpcomingList.setNestedScrollingEnabled(false);
    }
    /*public void swipeToRefresh(){
        // SwipeRefreshLayout

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        */

    private void getPopulerMovies() {
        Call<PopularMovieResponse> call = apiInterface.getPopulerMovies(Constant.API_KEY);
        call.enqueue(new Callback<PopularMovieResponse>() {
            @Override
            public void onResponse(Call<PopularMovieResponse> call, Response<PopularMovieResponse> response) {
                popularMovieResponseResultsList = response.body().getResults();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerViewMovieList.setLayoutManager(linearLayoutManager);
                checkFavoriteMoviesInList();
                movieListRecyclerAdapter = new MovieListRecyclerAdapter(popularMovieResponseResultsList, MovieListFragment.this);
                recyclerViewMovieList.setAdapter(movieListRecyclerAdapter);
                movieListRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PopularMovieResponse> call, Throwable t) {
                Log.d("xx", "xx");
            }
        });
    }

    private void getTopRatedMovies() {
        Call<TopRatedMovieResponse> call = apiInterface.getTopRatedMovies(Constant.API_KEY);
        call.enqueue(new Callback<TopRatedMovieResponse>() {
            @Override
            public void onResponse(Call<TopRatedMovieResponse> call, Response<TopRatedMovieResponse> response) {
                topRatedMovieResponseResultsList = response.body().getResults();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerViewTopRatedList.setLayoutManager(linearLayoutManager);
                topRatedMovieRecyclerAdapter = new TopRatedMovieRecyclerAdapter(topRatedMovieResponseResultsList, MovieListFragment.this);
                recyclerViewTopRatedList.setAdapter(topRatedMovieRecyclerAdapter);
                topRatedMovieRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TopRatedMovieResponse> call, Throwable t) {
                Log.d("anilisgor", t.getMessage());
            }

        });
    }

    private void getUpcomingMovies() {
        Call<UpcomingResponse> call = apiInterface.getUpcomingMovies(Constant.API_KEY);
        call.enqueue(new Callback<UpcomingResponse>() {
            @Override
            public void onResponse(Call<UpcomingResponse> call, Response<UpcomingResponse> response) {
                upcomingResponseResultsList = response.body().getResults();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerViewUpcomingList.setLayoutManager(linearLayoutManager);
                upcomingMovieRecyclerAdapter = new UpcomingMovieRecyclerAdapter(upcomingResponseResultsList, MovieListFragment.this);
                recyclerViewUpcomingList.setAdapter(upcomingMovieRecyclerAdapter);
                upcomingMovieRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<UpcomingResponse> call, Throwable t) {

            }

        });
    }

    private void getPopulerPeople() {
        Call<PersonPopularResponse> call = apiInterface.getPopulerPeople(Constant.API_KEY);
        call.enqueue(new Callback<PersonPopularResponse>() {
            @Override
            public void onResponse(Call<PersonPopularResponse> call, Response<PersonPopularResponse> response) {
                personPopulerResponseResultsList = response.body().getResults();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerViewPopulerPeopleList.setLayoutManager(linearLayoutManager);
                castPopularPeopleRecyclerAdapter = new CastPopularPeopleRecyclerAdapter(personPopulerResponseResultsList, MovieListFragment.this);
                recyclerViewPopulerPeopleList.setAdapter(castPopularPeopleRecyclerAdapter);
                castPopularPeopleRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PersonPopularResponse> call, Throwable t) {

            }


        });
    }

    /*private void getPopulerMoviesWithSwipe() {
        mSwipeRefreshLayout.setRefreshing(true);
        Call<PopulerMovieResponse> call = apiInterface.getPopulerMovies(Constant.API_KEY);
        call.enqueue(new Callback<PopulerMovieResponse>() {
            @Override
            public void onResponse(Call<PopulerMovieResponse> call, Response<PopulerMovieResponse> response) {
                populerMovieResponseResultsList = response.body().getResults();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
                recyclerViewMovieList.setLayoutManager(linearLayoutManager);
                checkFavoriteMoviesInList();
                movieListRecyclerAdapter = new MovieListRecyclerAdapter(populerMovieResponseResultsList, MovieListFragment.this);
                recyclerViewMovieList.setAdapter(movieListRecyclerAdapter);

                movieListRecyclerAdapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<PopulerMovieResponse> call, Throwable t) {
                Log.d("xx", "xx");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }*/
    public void checkFavoriteMoviesInList() {
        for (PopularMovieResponseResults populerMovie : popularMovieResponseResultsList) {
            FavoriteMovie favoriteMovie2 = mRealm.where(FavoriteMovie.class).equalTo("movieID", populerMovie.getİd()).findFirst();
            if (favoriteMovie2 != null) {
                populerMovie.setFavorite(true);
            }
        }
    }

    @Override
    public void favoriteMovie(PopularMovieResponseResults populerResponse) {

        if (!mRealm.isInTransaction()) {
            mRealm.beginTransaction();
        }
        FavoriteMovie favoriteMovie = new FavoriteMovie();
        favoriteMovie.setMovieID(populerResponse.getİd());
        favoriteMovie.setMovieOverview(populerResponse.getOverview());
        favoriteMovie.setMovieTitle(populerResponse.getTitle());
        favoriteMovie.setMoviewPosterpath(populerResponse.getPoster_path());
        favoriteMovie.setBackdrop_path(populerResponse.getBackdrop_path());
        favoriteMovie.setRelease_date(populerResponse.getRelease_date());
        favoriteMovie.setVote_average(populerResponse.getVote_average());

        FavoriteMovie favoriteMovie2 = mRealm.where(FavoriteMovie.class).equalTo("movieID", populerResponse.getİd()).findFirst();
        if (favoriteMovie2 != null) {
            favoriteMovie2.deleteFromRealm();
            mRealm.commitTransaction();
        } else {
            mRealm.insertOrUpdate(favoriteMovie);
            mRealm.commitTransaction();
        }
    }



  /*  @Override
    public void detailMovie(PopulerMovieResponseResults populerResponse) {
        mMovieListFragmentEventListener.detailMovieShow(populerResponse);
    }
*/


}
