package com.alacritysoftware.moviecenter.ui.fragments.moviedetail.casts;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.CastMovieRecyclerAdapter;
import com.alacritysoftware.moviecenter.model.CastsMovieResponse;
import com.alacritysoftware.moviecenter.model.CastsMovieResponseCast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InsideDetailCastFragment extends Fragment implements CastMovieRecyclerAdapter.CastMovieRecyclerAdapterListener{
    private int movie_id;
    private APIInterface apiInterface;
    private RecyclerView recyclerViewCastList;
    private CastMovieRecyclerAdapter castMovieRecyclerAdapter;
    private List<CastsMovieResponseCast> castsMovieResponseCastsList;
    private InsideDetailCastFragmentEventListener mInsideDetailCastFragmentEventListener;

    public interface InsideDetailCastFragmentEventListener {
        void detailPersonSend(int person_id);
    }


    public InsideDetailCastFragment newInstance(InsideDetailCastFragmentEventListener insideDetailCastFragmentEventListener ) {
        InsideDetailCastFragment fragment = new InsideDetailCastFragment();
        fragment.mInsideDetailCastFragmentEventListener= insideDetailCastFragmentEventListener;
        return fragment;
    }
    @Override
    public void detailPerson(int person_id) {
        mInsideDetailCastFragmentEventListener.detailPersonSend(person_id);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public void init(View view) {
        castsMovieResponseCastsList = new ArrayList<>();
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        recyclerViewCastList = view.findViewById(R.id.recyclerViewCastList);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inside_detail_cast, container, false);
        movie_id =  getArguments().getInt("movie_id");
        init(view);
        getCastMovies();
        return view;
    }
    private void getCastMovies() {
        Call<CastsMovieResponse> call = apiInterface.getCastMovie(movie_id, Constant.API_KEY);
        call.enqueue(new Callback<CastsMovieResponse>() {
            @Override
            public void onResponse(Call<CastsMovieResponse> call, Response<CastsMovieResponse> response) {
                castsMovieResponseCastsList = response.body().getCastsMovieResponseCasts();
                recyclerViewCastList.setLayoutManager(new LinearLayoutManager(getActivity()));
                castMovieRecyclerAdapter = new CastMovieRecyclerAdapter(castsMovieResponseCastsList, InsideDetailCastFragment.this);
                recyclerViewCastList.setAdapter(castMovieRecyclerAdapter);
                castMovieRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<CastsMovieResponse> call, Throwable t) {

            }
        });
    }


}
