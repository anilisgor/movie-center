package com.alacritysoftware.moviecenter.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.alacritysoftware.moviecenter.ui.fragments.favorites.FavoriteFragment;
import com.alacritysoftware.moviecenter.ui.fragments.moviedetail.main.MovieDetailFragment;
import com.alacritysoftware.moviecenter.ui.fragments.moviesearch.MovieSearchFragment;
import com.alacritysoftware.moviecenter.ui.fragments.moviecasts.main.PersonDetailFragment;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.ui.fragments.movielist.MovieListFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements MovieListFragment.MovieListFragmentEventListener,
        MovieDetailFragment.MovieDetailFragmentEventListener,
        MovieSearchFragment.MovieSearchFragmentEventListener,
        FavoriteFragment.FavoriteFragmentEventListener,
        PersonDetailFragment.PersonDetailFragmentEventListener {
    BottomNavigationView bottomNav;
    final Fragment fragment1 = new MovieListFragment().newInstance(MainActivity.this);
    final Fragment fragment2 = new MovieSearchFragment().newInstance(MainActivity.this);
    final Fragment fragment3 = new FavoriteFragment().newInstance(MainActivity.this);
    final FragmentManager fm = getSupportFragmentManager();
    Fragment currentFragment = fragment1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        fm.beginTransaction().add(R.id.fragment_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.fragment_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.fragment_container,fragment1, "1").commit();
        /*if (savedInstanceState == null) {
            fm.beginTransaction().replace(R.id.fragment_container,
                    new MovieListFragment().newInstance(MainActivity.this)).commit();
        }*/

    }

 private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    //Fragment selectedFragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.nav_Movielist:
                            fm.beginTransaction().hide(currentFragment).show(fragment1).commit();
                            currentFragment = fragment1;
                            return true;
                        case R.id.nav_MovieSearch:
                            fm.beginTransaction().hide(currentFragment).show(fragment2).commit();
                            currentFragment = fragment2;
                            return true;
                        case R.id.nav_MovieFav:
                            fm.beginTransaction().hide(currentFragment).show(fragment3).commit();
                            currentFragment = fragment3;
                            return true;

                    }
                    return false;
                }


            };

   /* private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.nav_Movielist:
                            selectedFragment = new MovieListFragment().newInstance(MainActivity.this);
                            break;
                        case R.id.nav_MovieSearch:
                            selectedFragment = new MovieSearchFragment().newInstance(MainActivity.this);
                            break;
                        case R.id.nav_MovieFav:
                            selectedFragment = new FavoriteFragment().newInstance(MainActivity.this);
                            break;

                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).addToBackStack(null).commit();
                    return true;
                }


            };*/

    public void callFragments(Fragment fragmentCall){
        fm.beginTransaction().add(R.id.fragment_container,fragmentCall).addToBackStack(null).commit();
        bottomNav.setVisibility(View.GONE);
        fm.beginTransaction().hide(currentFragment).commit();
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();

        } else if (count == 1) {
            getSupportFragmentManager().popBackStack();
            bottomNav.setVisibility(View.VISIBLE);
            fm.beginTransaction().show(currentFragment).commit();
        }
        else{
            getSupportFragmentManager().popBackStack();
            //fm.beginTransaction().show(currentFragment).commit();
        }
    }
    @Override
    public void detailMoviesShowSer(int movie_id) {
        Fragment fragment = new MovieDetailFragment().newInstance(MainActivity.this,movie_id);
        callFragments(fragment);

    }

    @Override
    public void detailMovieShowFav(int movie_id) {
        Fragment fragment = new MovieDetailFragment().newInstance(MainActivity.this,movie_id);
        callFragments(fragment);
    }

    @Override
    public void detailMovieShow(int movie_id) {
        Fragment fragment = new MovieDetailFragment().newInstance(MainActivity.this,movie_id);
        callFragments(fragment);

    }

    @Override
    public void detailPersonShow(int person_id) {
        Fragment fragment = new PersonDetailFragment().newInstance(MainActivity.this,person_id);
        callFragments(fragment);
    }


    @Override
    public void onDetailMovieFromInsideClick(int movie_id) {
        Fragment fragment = new MovieDetailFragment().newInstance(MainActivity.this,movie_id);
        //fm.beginTransaction().hide(fragment).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).addToBackStack(null).commit();

    }


    @Override
    public void onDetailPersonFromInsideClick(int person_id) {
        Fragment fragment = new PersonDetailFragment().newInstance(MainActivity.this,person_id);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).addToBackStack(null).commit();
    }


    @Override
    public void onDetailCastMovieFromInsideClick(int movie_id) {
        Fragment fragment = new MovieDetailFragment().newInstance(MainActivity.this,movie_id);
        //fm.beginTransaction().hide(fragment).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).addToBackStack(null).commit();
    }
}
