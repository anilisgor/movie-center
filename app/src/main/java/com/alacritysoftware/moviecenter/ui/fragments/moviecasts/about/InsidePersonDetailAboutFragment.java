package com.alacritysoftware.moviecenter.ui.fragments.moviecasts.about;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.model.PeopleDetailResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InsidePersonDetailAboutFragment extends Fragment {
    private TextView text_explanation, text_birthday, text_placeofbirth, text_imdbid, text_homepage, text_deathday;
    private APIInterface apiInterface;
    private List<PeopleDetailResponse> peopleDetailResponseList;
    private InsidePersonDetailAboutFragmentEventListener mInsidePersonDetailAboutFragmentEventListener;
    private int person_id;
    public InsidePersonDetailAboutFragment() {
        // Required empty public constructor
    }
    public interface InsidePersonDetailAboutFragmentEventListener {

    }
    public static InsidePersonDetailAboutFragment newInstance(InsidePersonDetailAboutFragmentEventListener insideDetailFragmentEventListener) {
        InsidePersonDetailAboutFragment fragment = new InsidePersonDetailAboutFragment();
        fragment.mInsidePersonDetailAboutFragmentEventListener = insideDetailFragmentEventListener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inside_person_detail_about, container, false);
        init(view);
        person_id =  getArguments().getInt("person_id");
        getPeopleDetail();
        return view;
    }

    public void init(View view) {
        text_explanation = view.findViewById(R.id.text_explanation);
        text_birthday = view.findViewById(R.id.text_birthday);
        text_placeofbirth = view.findViewById(R.id.text_placeofbirth);
        text_imdbid = view.findViewById(R.id.text_imdbid);
        text_homepage = view.findViewById(R.id.text_homepage);
        text_imdbid = view.findViewById(R.id.text_imdbid);
        text_deathday = view.findViewById(R.id.text_deathday);

        apiInterface = ApiClient.getClient().create(APIInterface.class);
        peopleDetailResponseList = new ArrayList<>();

    }

    public void getPeopleDetail() {
        Call<PeopleDetailResponse> call = apiInterface.getPeopleDetail(person_id, Constant.API_KEY);
        call.enqueue(new Callback<PeopleDetailResponse>() {

            @Override
            public void onResponse(Call<PeopleDetailResponse> call, Response<PeopleDetailResponse> response) {
                if (response.isSuccessful()) {
                    text_explanation.setText(response.body().getBiography());
                    text_birthday.setText(response.body().getBirthday());
                    text_placeofbirth.setText(response.body().getPlace_of_birth());
                    text_homepage.setText((CharSequence) response.body().getHomepage());
                    text_imdbid.setText("https://www.imdb.com/title/"+response.body().getİmdb_id());
                    text_deathday.setText(response.body().getDeathday());
                }
            }

            @Override
            public void onFailure(Call<PeopleDetailResponse> call, Throwable t) {

            }


        });
    }
}
