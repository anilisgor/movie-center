package com.alacritysoftware.moviecenter.ui.fragments.moviecasts.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;


import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.SlidingImageAdapter;
import com.alacritysoftware.moviecenter.adapter.ViewPagerPeopleAdapter;
import com.alacritysoftware.moviecenter.model.PeopleDetailResponse;
import com.alacritysoftware.moviecenter.model.PeopleImagesResponse;
import com.alacritysoftware.moviecenter.model.PeopleImagesResponseProfiles;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PersonDetailFragment extends Fragment implements ViewPagerPeopleAdapter.ViewPagerPeopleAdapterListener {

    private APIInterface apiInterface;
    private PersonDetailFragmentEventListener mPersonDetailFragmentEventListener;
    private TextView text_header;
    private ImageView image_movie;
    private int personId =  0;
    private TabLayout tabLayout;
    private int blurEffectDegree = 10;
    private ViewPager viewPager,viewPagerImage;
    private ViewPagerPeopleAdapter viewPagerAdapter;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private ArrayList<String> ImagesArray = new ArrayList<String>();
    private List<PeopleImagesResponseProfiles> peopleImagesResponseProfilesList;
    private CirclePageIndicator indicator;
    private static int NUM_PAGES = 0;
    private static int currentPage = 0;



    @Override
    public void onDetailCastMovieFromInsideClick(int movie_id) {
        mPersonDetailFragmentEventListener.onDetailCastMovieFromInsideClick(movie_id);
    }

    public interface  PersonDetailFragmentEventListener{
        void onDetailCastMovieFromInsideClick(int movie_id);
    }

    public static PersonDetailFragment newInstance(PersonDetailFragmentEventListener personDetailFragmentEventListener,int person_id) {
        PersonDetailFragment fragment = new PersonDetailFragment();
        fragment.mPersonDetailFragmentEventListener= personDetailFragmentEventListener;
        fragment.personId = person_id;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_person_detail, container, false);
        init(view);


        getPeopleImages();
        getDetailPerson();
        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        }

    public void init(View view){
        toolbar = view.findViewById(R.id.toolbar);
        text_header = view.findViewById(R.id.text_header);
        //image_movie = view.findViewById(R.id.image_movie);
        viewPagerImage = view.findViewById(R.id.viewPagerImage);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("About"));
        tabLayout.addTab(tabLayout.newTab().setText("Related Movies"));
        peopleImagesResponseProfilesList =  new ArrayList<>();
        indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        viewPagerAdapter = new ViewPagerPeopleAdapter(getChildFragmentManager(),tabLayout.getTabCount(),personId, this);
        viewPager.setAdapter(viewPagerAdapter);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        collapsingToolbar = (CollapsingToolbarLayout) view.findViewById(R.id.toolbar_layout);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.TextAppearance_MyApp_Title_Collapsed);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.TextAppearance_MyApp_Title_Expanded);
        collapsingToolbar.setTitle(" ");
        appBarLayout = (AppBarLayout) view.findViewById(R.id.app_bar);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //image_movie.setClipToOutline(true);
        apiInterface = ApiClient.getClient().create(APIInterface.class);



    }
    private void initViewPageImages(){

        viewPagerImage.setAdapter(new SlidingImageAdapter(requireContext(),ImagesArray));

        indicator.setViewPager(viewPagerImage);
        final float density = getResources().getDisplayMetrics().density;

        indicator.setRadius(5 * density);

        NUM_PAGES =peopleImagesResponseProfilesList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPagerImage.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    private void getDetailPerson(){
        Call<PeopleDetailResponse> call = apiInterface.getPeopleDetail(personId, Constant.API_KEY);
        call.enqueue(new Callback<PeopleDetailResponse>() {
            @Override
            public void onResponse(Call<PeopleDetailResponse> call, Response<PeopleDetailResponse> response) {
                text_header.setText(response.body().getName());
                // text_explanation.setText(response.body().getOverview());
                /*Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/w500"+response.body().getProfile_path()).error(R.drawable.no_profile_photo)
                        .into(image_movie);*/

                appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                    boolean isShow = true;
                    int scrollRange = -1;

                    @Override
                    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                        if (scrollRange == -1) {
                            scrollRange = appBarLayout.getTotalScrollRange();
                        }
                        if (scrollRange + verticalOffset == 0) {
                            //when collapsingToolbar at that time display actionbar title
                            collapsingToolbar.setTitle(" "+response.body().getName()+" ");
                            isShow = true;
                        } else if (isShow) {
                            //carefull there must a space between double quote otherwise it dose't work
                            collapsingToolbar.setTitle(" "+response.body().getName()+" ");
                            isShow = false;
                        }
                    }
                });
            }


            @Override
            public void onFailure(Call<PeopleDetailResponse> call, Throwable t) {
                Log.d("anilisgorss", t.getMessage());
            }

        });

    }
    private void getPeopleImages(){
        Call<PeopleImagesResponse> call = apiInterface.getPeopleImagesDetail(personId, Constant.API_KEY);
        call.enqueue(new Callback<PeopleImagesResponse>() {
            @Override
            public void onResponse(Call<PeopleImagesResponse> call, Response<PeopleImagesResponse> response) {
                peopleImagesResponseProfilesList = response.body().getProfiles();

                for(int i=0;i<peopleImagesResponseProfilesList.size();i++)
                    ImagesArray.add(peopleImagesResponseProfilesList.get(i).getFile_path());
                initViewPageImages();
                /*popularMovieResponseResultsList = response.body().getResults();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerViewMovieList.setLayoutManager(linearLayoutManager);
                checkFavoriteMoviesInList();
                movieListRecyclerAdapter = new MovieListRecyclerAdapter(popularMovieResponseResultsList, MovieListFragment.this);
                recyclerViewMovieList.setAdapter(movieListRecyclerAdapter);
                movieListRecyclerAdapter.notifyDataSetChanged();*/
            }

            @Override
            public void onFailure(Call<PeopleImagesResponse> call, Throwable t) {

            }
        });
    }




}
