package com.alacritysoftware.moviecenter.ui.fragments.moviedetail.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.local.FavoriteMovie;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.ViewPagerAdapter;
import com.alacritysoftware.moviecenter.model.DetailMoviesResponse;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import jp.wasabeef.glide.transformations.BlurTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MovieDetailFragment extends Fragment implements ViewPagerAdapter.ViewPagerAdapterEventListener {

    private List<FavoriteMovie> favoriteMovieList;
    private APIInterface apiInterface;
    private MovieDetailFragmentEventListener mMovieDetailFragmentEventListener;
    private TextView text_header,text_vote_average;
    private ImageView image_movie,image_movie2,image_favbutton;
    private int blurEffectDegree = 10;
    private SimpleDateFormat format;
    private Realm mRealm;
    private int movieId =  0;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;

    public interface  MovieDetailFragmentEventListener{
        void onDetailMovieFromInsideClick(int movie_id);
        void onDetailPersonFromInsideClick(int person_id);
    }
    @Override
    public void onDetailMovieFromInsideClick(int movie_id) {
        mMovieDetailFragmentEventListener.onDetailMovieFromInsideClick(movie_id);
    }

    @Override
    public void onDetailPersonFromClick(int person_id) {
        mMovieDetailFragmentEventListener.onDetailPersonFromInsideClick(person_id);
    }



    public static MovieDetailFragment newInstance(MovieDetailFragmentEventListener movieDetailFragmentEventListener,
                                                  int movie_id) {
        MovieDetailFragment fragment = new MovieDetailFragment();
        fragment.mMovieDetailFragmentEventListener = movieDetailFragmentEventListener;
        fragment.movieId = movie_id;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        init(view);
        getDetailMovie();
        getDataFromLocalDb();
        checkFavoriteMoviesInList();
        pushFavoriteButton(view);

        return view;
    }
    public void init(View view){

        toolbar = view.findViewById(R.id.toolbar);
        text_header = view.findViewById(R.id.text_header);
        image_movie = view.findViewById(R.id.image_movie);
        image_movie2 = view.findViewById(R.id.image_movie2);
        image_favbutton = view.findViewById(R.id.image_favbutton);
        text_vote_average = view.findViewById(R.id.text_vote_average);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("About"));
        tabLayout.addTab(tabLayout.newTab().setText("Cast"));
        tabLayout.addTab(tabLayout.newTab().setText("Related Movies"));
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),tabLayout.getTabCount(),movieId, this);
        viewPager.setAdapter(viewPagerAdapter);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        collapsingToolbar = (CollapsingToolbarLayout) view.findViewById(R.id.toolbar_layout);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.TextAppearance_MyApp_Title_Collapsed);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.TextAppearance_MyApp_Title_Expanded);
        collapsingToolbar.setTitle(" ");
        appBarLayout = (AppBarLayout) view.findViewById(R.id.app_bar);


        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        image_movie.setClipToOutline(true);
        mRealm = Realm.getDefaultInstance();
        favoriteMovieList= new ArrayList<>();
        apiInterface = ApiClient.getClient().create(APIInterface.class);

    }

    private void getDetailMovie(){
        Call<DetailMoviesResponse> call = apiInterface.getDetailMovie(movieId, Constant.API_KEY);
        call.enqueue(new Callback<DetailMoviesResponse>() {

            @Override
            public void onResponse(Call<DetailMoviesResponse> call, Response<DetailMoviesResponse> response) {
                if(response.isSuccessful()){
                    text_header.setText(response.body().getTitle());
                   // text_explanation.setText(response.body().getOverview());
                    text_vote_average.setText(Html.fromHtml(String.valueOf(response.body().getVote_average())+"<small>"+"/10"+"</small>"));
                    Glide.with(requireContext())
                            .load("https://image.tmdb.org/t/p/w500"+response.body().getPoster_path())
                            .into(image_movie);
                    Glide.with(requireContext())
                            .load("https://image.tmdb.org/t/p/w500"+response.body().getBackdrop_path())
                            .transform(new BlurTransformation(blurEffectDegree))
                            .error(R.drawable.no_image_avaliable)
                            .into(image_movie2);
                    appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                        boolean isShow = true;
                        int scrollRange = -1;

                        @Override
                        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                            if (scrollRange == -1) {
                                scrollRange = appBarLayout.getTotalScrollRange();
                            }
                            if (scrollRange + verticalOffset == 0) {
                                //when collapsingToolbar at that time display actionbar title
                                collapsingToolbar.setTitle(getResources().getString(R.string.app_name));
                                isShow = true;
                            } else if (isShow) {
                                //carefull there must a space between double quote otherwise it dose't work
                                collapsingToolbar.setTitle(" "+response.body().getTitle()+" ");
                                isShow = false;
                            }
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<DetailMoviesResponse> call, Throwable t) {
            }
        });
    }



    public void checkFavoriteMoviesInList() {
        for(FavoriteMovie populerMovie : favoriteMovieList){
            if(movieId != 0)
            {
                if(populerMovie.getMovieID() == movieId){
                    image_favbutton.setImageResource(R.drawable.ic_star_black_24dp);
                }
            }

        }
    }

    private void getDataFromLocalDb() {
        RealmResults<FavoriteMovie> results = mRealm.where(FavoriteMovie.class).findAll();
        if (results != null && results.size() > 0) {
            favoriteMovieList.addAll(results);
        } else {
        }
    }

    public void pushFavoriteButton(View view){
        image_favbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mRealm.isInTransaction()) {
                    mRealm.beginTransaction();
                }
                Call<DetailMoviesResponse> call = apiInterface.getDetailMovie(movieId, Constant.API_KEY);
                call.enqueue(new Callback<DetailMoviesResponse>() {

                    @Override
                    public void onResponse(Call<DetailMoviesResponse> call, Response<DetailMoviesResponse> response) {
                        if(response.isSuccessful()){
                            if(movieId !=  0){
                                FavoriteMovie favoriteMovie = new FavoriteMovie();
                                favoriteMovie.setMovieID(response.body().getİd());
                                favoriteMovie.setMovieOverview(response.body().getOverview());
                                favoriteMovie.setMovieTitle(response.body().getTitle());
                                favoriteMovie.setMoviewPosterpath(response.body().getPoster_path());
                                favoriteMovie.setBackdrop_path(response.body().getBackdrop_path());
                                favoriteMovie.setRelease_date(response.body().getRelease_date());
                                favoriteMovie.setVote_average(response.body().getVote_average());

                                FavoriteMovie favoriteMovie2 = mRealm.where(FavoriteMovie.class).equalTo("movieID", response.body().getİd()).findFirst();
                                if (favoriteMovie2 != null) {
                                    image_favbutton.setImageResource(R.drawable.ic_star_border_black_24dp);
                                    favoriteMovie2.deleteFromRealm();
                                    mRealm.commitTransaction();
                                } else {
                                    image_favbutton.setImageResource(R.drawable.ic_star_black_24dp);
                                    mRealm.insertOrUpdate(favoriteMovie);
                                    mRealm.commitTransaction();
                                }
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<DetailMoviesResponse> call, Throwable t) {
                    }
                });
            }
        });

    }


}
