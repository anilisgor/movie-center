package com.alacritysoftware.moviecenter.ui.fragments.moviedetail.relatedmovies;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.RelatedMoviesRecyclerAdapter;
import com.alacritysoftware.moviecenter.model.SimilarMovieResponse;
import com.alacritysoftware.moviecenter.model.SimilarMovieResponseResults;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsideDetailRelatedMoviesFragment extends Fragment implements RelatedMoviesRecyclerAdapter.RelatedMoviesRecyclerAdapterListener {
    private int movieID;
    private RecyclerView recyclerViewRelatedMovieList;
    private APIInterface apiInterface;
    private List<SimilarMovieResponseResults> similarMovieResponseResultsList;
    private RelatedMoviesRecyclerAdapter relatedMoviesRecyclerAdapter;
    private InsideDetailRelatedMoviesFragmentEventListener  mInsideDetailRelatedMoviesFragmentEventListener;

    public interface InsideDetailRelatedMoviesFragmentEventListener {
        void detailMovieShowInside(int movie_id);
    }
    @Override
    public void detailMovieFromInside(int movie_id) {
        mInsideDetailRelatedMoviesFragmentEventListener.detailMovieShowInside(movie_id);
    }




    public InsideDetailRelatedMoviesFragment newInstance(InsideDetailRelatedMoviesFragmentEventListener  insideDetailRelatedMoviesFragmentEventListener) {
        InsideDetailRelatedMoviesFragment fragment = new InsideDetailRelatedMoviesFragment();
        fragment.mInsideDetailRelatedMoviesFragmentEventListener =  insideDetailRelatedMoviesFragmentEventListener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inside_detail_related_movies, container, false);
        movieID =  getArguments().getInt("movieID");
        init(view);
        getRelatedMovies();
        return view;
    }
    public void init(View view) {
        similarMovieResponseResultsList= new ArrayList<>();
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        recyclerViewRelatedMovieList = view.findViewById(R.id.recyclerViewRelatedMovieList);
    }
    public void getRelatedMovies(){
        Call<SimilarMovieResponse> call = apiInterface.getSimilarMovie(movieID, Constant.API_KEY);
        call.enqueue(new Callback<SimilarMovieResponse>() {
            @Override
            public void onResponse(Call<SimilarMovieResponse> call, Response<SimilarMovieResponse> response) {
                similarMovieResponseResultsList = response.body().getResults();
                //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(),3);
                recyclerViewRelatedMovieList.setLayoutManager(linearLayoutManager);
                relatedMoviesRecyclerAdapter = new RelatedMoviesRecyclerAdapter(similarMovieResponseResultsList, InsideDetailRelatedMoviesFragment.this);
                recyclerViewRelatedMovieList.setAdapter(relatedMoviesRecyclerAdapter);
                relatedMoviesRecyclerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<SimilarMovieResponse> call, Throwable t) {
            }


        });
    }


}
