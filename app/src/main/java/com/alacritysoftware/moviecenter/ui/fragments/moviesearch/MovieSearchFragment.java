package com.alacritysoftware.moviecenter.ui.fragments.moviesearch;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.local.FavoriteMovie;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.SearchMovieRecyclerAdapter;
import com.alacritysoftware.moviecenter.model.SearchMovieResponse;
import com.alacritysoftware.moviecenter.model.SearchMovieResponseResults;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MovieSearchFragment extends Fragment implements SearchMovieRecyclerAdapter.SearchMovieRecyclerAdapterListener {
    RecyclerView recyclerViewSearchList;
    SearchMovieRecyclerAdapter searchMovieRecyclerAdapter;
    List<SearchMovieResponseResults> searchMovieResponseResultsList;
    MovieSearchFragmentEventListener mMovieListFragmentEventListener;
    APIInterface apiInterface;
    SearchView searchMovie;
    Realm mRealm;

    public interface MovieSearchFragmentEventListener {
        void detailMoviesShowSer(int movie_id);
    }

    public MovieSearchFragment newInstance(MovieSearchFragmentEventListener movieSearchFragmentEventListener) {
        MovieSearchFragment fragment = new MovieSearchFragment();
        fragment.mMovieListFragmentEventListener = movieSearchFragmentEventListener;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_search, container, false);
        init(view);
        searchResult();

        return view;
    }
    private void init(View view) {
        recyclerViewSearchList = view.findViewById(R.id.recyclerViewSearchList);
        searchMovie = view.findViewById(R.id.searchMovie);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        searchMovieResponseResultsList= new ArrayList<>();
        mRealm = Realm.getDefaultInstance();
        //recyclerViewSearchList.setLayoutManager(new LinearLayoutManager(getActivity()));
        //searchMovieRecyclerAdapter = new SearchMovieRecyclerAdapter(searchMovieResponseResultsList);
        //recyclerViewSearchList.setAdapter(searchMovieRecyclerAdapter);
    }
    private void getSearchMovie(String query){
        Call<SearchMovieResponse> call = apiInterface.getSearchMovie(Constant.API_KEY, query);
        call.enqueue(new Callback<SearchMovieResponse>() {
            @Override
            public void onResponse(Call<SearchMovieResponse> call, Response<SearchMovieResponse> response) {
                searchMovieResponseResultsList = response.body().getResults();
                recyclerViewSearchList.setLayoutManager(new LinearLayoutManager(getActivity()));
                searchMovieRecyclerAdapter = new SearchMovieRecyclerAdapter(searchMovieResponseResultsList,MovieSearchFragment.this);
                recyclerViewSearchList.setAdapter(searchMovieRecyclerAdapter);
                checkFavoriteMoviesInList();
                searchMovieRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<SearchMovieResponse> call, Throwable t) {
                Log.d("anilisgorss", t.getMessage());
            }


        });
    }
    public void searchResult(){
        searchMovie.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               getSearchMovie(query);
               searchMovie.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }
    public void checkFavoriteMoviesInList() {
        for(SearchMovieResponseResults populerMovie : searchMovieResponseResultsList){
            FavoriteMovie favoriteMovie2 = mRealm.where(FavoriteMovie.class).equalTo("movieID", populerMovie.getİd()).findFirst();
            if(favoriteMovie2 != null){
                populerMovie.setFavorite(true);
            }
        }
    }
    @Override
    public void searchMovieResult(SearchMovieResponseResults populerResponse) {
        if (!mRealm.isInTransaction()) {
            mRealm.beginTransaction();
        }
        FavoriteMovie favoriteMovie = new FavoriteMovie();
        favoriteMovie.setMovieID(populerResponse.getİd());
        favoriteMovie.setMovieOverview(populerResponse.getOverview());
        favoriteMovie.setMovieTitle(populerResponse.getTitle());
        favoriteMovie.setMoviewPosterpath(populerResponse.getPoster_path());
        favoriteMovie.setBackdrop_path(populerResponse.getBackdrop_path());
        favoriteMovie.setRelease_date(populerResponse.getRelease_date());
        favoriteMovie.setVote_average(populerResponse.getVote_average());

        FavoriteMovie favoriteMovie2 = mRealm.where(FavoriteMovie.class).equalTo("movieID", populerResponse.getİd()).findFirst();
        if (favoriteMovie2 != null) {
            favoriteMovie2.deleteFromRealm();
            mRealm.commitTransaction();
        } else {
            mRealm.insertOrUpdate(favoriteMovie);
            mRealm.commitTransaction();
        }
    }

    @Override
    public void detailMovieFromSearch(int movie_id) {
        mMovieListFragmentEventListener.detailMoviesShowSer(movie_id);
    }
}
