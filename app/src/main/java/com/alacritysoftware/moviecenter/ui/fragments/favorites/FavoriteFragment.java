package com.alacritysoftware.moviecenter.ui.fragments.favorites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alacritysoftware.moviecenter.local.FavoriteMovie;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.FavoriteMovieRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class FavoriteFragment extends Fragment implements FavoriteMovieRecyclerAdapter.FavoriteMovieRecyclerAdapterListener {
    Realm mRealm;
    RecyclerView recyclerViewFavoriteList;
    List<FavoriteMovie> favoriteMovieList;
    FavoriteMovieRecyclerAdapter favoriteMovieRecyclerAdapter;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onResume() {
        super.onResume();
        favoriteMovieRecyclerAdapter.notifyDataSetChanged();
    }

    FavoriteFragmentEventListener mFavoriteFragmentEventListener;

    public interface FavoriteFragmentEventListener{
        void detailMovieShowFav(int movie_id);
    }


    public FavoriteFragment newInstance(FavoriteFragmentEventListener favoriteFragmentEventListener) {
        FavoriteFragment fragment = new FavoriteFragment();
        fragment.mFavoriteFragmentEventListener = favoriteFragmentEventListener;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        // Inflate the layout for this fragment
        init(view);
        getDataFromLocalDb();
        swipeGetDataFromLocalDb();
        return view;
    }

    private void init(View view) {
        recyclerViewFavoriteList = view.findViewById(R.id.recyclerViewFavoriteList);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        favoriteMovieList = new ArrayList<>();
        mRealm = Realm.getDefaultInstance();
        recyclerViewFavoriteList.setLayoutManager(new LinearLayoutManager(getActivity()));
        favoriteMovieRecyclerAdapter = new FavoriteMovieRecyclerAdapter(favoriteMovieList, FavoriteFragment.this);
        recyclerViewFavoriteList.setAdapter(favoriteMovieRecyclerAdapter);


    }
    public void swipeGetDataFromLocalDb(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RealmResults<FavoriteMovie> results = mRealm.where(FavoriteMovie.class).findAll();
                if (results != null && results.size() > 0) {
                    for (int i = 0; i < results.size(); i++) {
                        FavoriteMovie f = results.get(i);
                        if(favoriteMovieList.contains(f)){

                        }
                        else{
                            favoriteMovieList.add(f);
                        }

                    }
                } else {
                }
                favoriteMovieRecyclerAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void getDataFromLocalDb() {
        RealmResults<FavoriteMovie> results = mRealm.where(FavoriteMovie.class).findAll();
        if (results != null && results.size() > 0) {
            for (int i = 0; i < results.size(); i++) {
                FavoriteMovie f = results.get(i);
                favoriteMovieList.add(f);
            }
        } else {
        }
        favoriteMovieRecyclerAdapter.notifyDataSetChanged();
    }


    @Override
    public void onFavoriteClick(FavoriteMovie favoriteMovie) {
        if (!mRealm.isInTransaction()) {
            mRealm.beginTransaction();
        }

        FavoriteMovie favoriteMovie2 = mRealm.where(FavoriteMovie.class).equalTo("movieID", favoriteMovie.getMovieID()).findFirst();
        if (favoriteMovie2.isValid()) {
            favoriteMovie.deleteFromRealm();
            mRealm.commitTransaction();
        }
    }

    @Override
    public void detailMovie(int movie_id) {
        mFavoriteFragmentEventListener.detailMovieShowFav(movie_id);
    }

    /*@Override
    public void detailMovie( favoriteMovie) {
        mFavoriteFragmentEventListener.detailMovieShow(favoriteMovie);
    }*/
}
