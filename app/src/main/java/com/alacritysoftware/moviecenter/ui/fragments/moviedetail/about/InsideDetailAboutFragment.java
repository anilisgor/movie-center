package com.alacritysoftware.moviecenter.ui.fragments.moviedetail.about;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.TypeMovieRecyclerAdapter;
import com.alacritysoftware.moviecenter.model.DetailMoviesResponse;
import com.alacritysoftware.moviecenter.model.DetailMoviesResponseGenres;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InsideDetailAboutFragment extends Fragment implements TypeMovieRecyclerAdapter.TypeMovieRecyclerAdapterListener {

    private InsideDetailFragmentEventListener mInsideDetailFragmentEventListener;
    private int movie_id;
    private DecimalFormat decim = new DecimalFormat("#,###.##");

    private TextView text_explanation,text_originaltitle,text_status,text_runtime,text_imdbid,text_vountcount,text_budget,text_release_date;
    private APIInterface apiInterface;
    private List<DetailMoviesResponseGenres> detailMoviesResponseGenresList;
    private RecyclerView recyclerViewTypesList;
    private TypeMovieRecyclerAdapter typeMovieRecyclerAdapter;
    public InsideDetailAboutFragment() {
        // Required empty public constructor
    }

    public static InsideDetailAboutFragment newInstance(InsideDetailFragmentEventListener insideDetailFragmentEventListener) {
        InsideDetailAboutFragment fragment = new InsideDetailAboutFragment();
        fragment.mInsideDetailFragmentEventListener = insideDetailFragmentEventListener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inside_detail_about, container, false);

        movie_id =  getArguments().getInt("movie_id");
        init(view);
        getDetailMovie();
        getDetailMovieTypes();

        return view;
    }
    public void init(View view) {
        text_status = view.findViewById(R.id.text_status);
        text_release_date = view.findViewById(R.id.text_release_date);
        text_budget = view.findViewById(R.id.text_budget);
        text_explanation = view.findViewById(R.id.text_explanation);
        text_originaltitle = view.findViewById(R.id.text_originaltitle);
        text_imdbid = view.findViewById(R.id.text_imdbid);
        text_runtime = view.findViewById(R.id.text_runtime);
        text_vountcount = view.findViewById(R.id.text_vountcount);

        apiInterface = ApiClient.getClient().create(APIInterface.class);
        recyclerViewTypesList = view.findViewById(R.id.recyclerViewTypesList);
        detailMoviesResponseGenresList = new ArrayList<>();

    }

    public void getDetailMovie() {
        Call<DetailMoviesResponse> call = apiInterface.getDetailMovie(movie_id, Constant.API_KEY);
        call.enqueue(new Callback<DetailMoviesResponse>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<DetailMoviesResponse> call, Response<DetailMoviesResponse> response) {
                if (response.isSuccessful()) {
                    text_explanation.setText(response.body().getOverview());
                    text_originaltitle.setText(response.body().getOriginal_title());
                    text_status.setText(response.body().getStatus());
                    text_runtime.setText(response.body().getRuntime()+" min.");
                    text_imdbid.setText("https://www.imdb.com/title/"+response.body().getİmdb_id());
                    text_vountcount.setText(String.valueOf(response.body().getVote_count()));
                    text_budget.setText("$"+decim.format(response.body().getBudget()));
                    text_release_date.setText(response.body().getRelease_date());

                }
            }

            @Override
            public void onFailure(Call<DetailMoviesResponse> call, Throwable t) {
            }
        });
    }
    public void getDetailMovieTypes(){
        Call<DetailMoviesResponse> call = apiInterface.getDetailMovie(movie_id,Constant.API_KEY);
        call.enqueue(new Callback<DetailMoviesResponse>() {
            @Override
            public void onResponse(Call<DetailMoviesResponse> call, Response<DetailMoviesResponse> response) {
                detailMoviesResponseGenresList = response.body().getGenres();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
                recyclerViewTypesList.setLayoutManager(linearLayoutManager);
                typeMovieRecyclerAdapter = new TypeMovieRecyclerAdapter(detailMoviesResponseGenresList,InsideDetailAboutFragment.this);
                recyclerViewTypesList.setAdapter(typeMovieRecyclerAdapter);
                typeMovieRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<DetailMoviesResponse> call, Throwable t) {

            }
        });
    }


    public interface InsideDetailFragmentEventListener {

    }
}
