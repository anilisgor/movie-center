package com.alacritysoftware.moviecenter.ui.fragments.moviecasts.inmovies;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.alacritysoftware.moviecenter.R;
import com.alacritysoftware.moviecenter.adapter.RelatedMoviesCastRecyclerAdapter;
import com.alacritysoftware.moviecenter.model.RelatedMovieCastResponse.Cast;
import com.alacritysoftware.moviecenter.model.RelatedMovieCastResponse.RelatedMovieCastResponse;
import com.alacritysoftware.moviecenter.remote.APIInterface;
import com.alacritysoftware.moviecenter.remote.ApiClient;
import com.alacritysoftware.moviecenter.utils.Constant;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsidePersonDetailMoviesInFragment extends Fragment implements RelatedMoviesCastRecyclerAdapter.RelatedMoviesCastRecyclerAdapterListener {
    private int personID;
    private RecyclerView recyclerViewRelatedMovieList;
    private APIInterface apiInterface;
    private List<Cast> castList;
    private RelatedMoviesCastRecyclerAdapter relatedMoviesCastRecyclerAdapter;
    private InsidePersonDetailMoviesInFragmentEventListener mInsidePersonDetailMoviesInFragmentEventListener;

    @Override
    public void detailcastMovieFromInside(int movie_id) {
        mInsidePersonDetailMoviesInFragmentEventListener.detailCastMovieShowInside(movie_id);
    }

    public interface InsidePersonDetailMoviesInFragmentEventListener {
        void detailCastMovieShowInside(int movie_id);
    }


    public InsidePersonDetailMoviesInFragment newInstance(InsidePersonDetailMoviesInFragmentEventListener insidePersonDetailMoviesInFragmentEventListener) {
        InsidePersonDetailMoviesInFragment fragment = new InsidePersonDetailMoviesInFragment();
        fragment.mInsidePersonDetailMoviesInFragmentEventListener = insidePersonDetailMoviesInFragmentEventListener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inside_person_detail_movies_in, container, false);
        personID =  getArguments().getInt("person_id");
        init(view);
        getRelatedMovies();
        return view;
    }
    public void init(View view) {
        castList= new ArrayList<>();
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        recyclerViewRelatedMovieList = view.findViewById(R.id.recyclerViewRelatedMovieList);
    }

    public void getRelatedMovies(){
        Call<RelatedMovieCastResponse> call = apiInterface.getSimilarCastMovie(personID, Constant.API_KEY);
        call.enqueue(new Callback<RelatedMovieCastResponse>() {
            @Override
            public void onResponse(Call<RelatedMovieCastResponse> call, Response<RelatedMovieCastResponse> response) {
                castList = response.body().getCast();
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(),3);
                recyclerViewRelatedMovieList.setLayoutManager(linearLayoutManager);
                relatedMoviesCastRecyclerAdapter = new RelatedMoviesCastRecyclerAdapter(castList, InsidePersonDetailMoviesInFragment.this);
                recyclerViewRelatedMovieList.setAdapter(relatedMoviesCastRecyclerAdapter);
                relatedMoviesCastRecyclerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<RelatedMovieCastResponse> call, Throwable t) {

            }



        });
    }
}
