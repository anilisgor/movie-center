package com.alacritysoftware.moviecenter.local;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FavoriteMovie extends RealmObject {
    @PrimaryKey
    private int movieID;
    private String movieTitle;
    private String movieOverview;
    private String moviewPosterpath;
    private String backdrop_path;
    private double vote_average;
    private String release_date;

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    private boolean isFavorite;

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMovieOverview() {
        return movieOverview;
    }

    public void setMovieOverview(String movieOverview) {
        this.movieOverview = movieOverview;
    }

    public String getMoviewPosterpath() {
        return moviewPosterpath;
    }

    public void setMoviewPosterpath(String moviewPosterpath) {
        this.moviewPosterpath = moviewPosterpath;
    }


}
