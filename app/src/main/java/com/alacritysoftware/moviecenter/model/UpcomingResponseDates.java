package com.alacritysoftware.moviecenter.model;

public class UpcomingResponseDates {
    private String maximum;
    private String minimum;

    public String getMaximum() {
        return this.maximum;
    }

    public void setMaximum(String maximum) {
        this.maximum = maximum;
    }

    public String getMinimum() {
        return this.minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }
}
