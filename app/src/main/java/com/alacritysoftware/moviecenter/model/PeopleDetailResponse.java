package com.alacritysoftware.moviecenter.model;

public class PeopleDetailResponse {
    private String[] also_known_as;
    private String birthday;
    private int gender;
    private String imdb_id;
    private String known_for_department;
    private String profile_path;
    private String biography;
    private String deathday;
    private String place_of_birth;
    private double popularity;
    private String name;
    private int id;
    private boolean adult;

    public String getDeathday() {
        return deathday;
    }

    public void setDeathday(String deathday) {
        this.deathday = deathday;
    }

    private Object homepage;

    public String[] getAlso_known_as() {
        return this.also_known_as;
    }

    public void setAlso_known_as(String[] also_known_as) {
        this.also_known_as = also_known_as;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return this.gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getİmdb_id() {
        return this.imdb_id;
    }

    public void setİmdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public String getKnown_for_department() {
        return this.known_for_department;
    }

    public void setKnown_for_department(String known_for_department) {
        this.known_for_department = known_for_department;
    }

    public String getProfile_path() {
        return this.profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public String getBiography() {
        return this.biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }




    public String getPlace_of_birth() {
        return this.place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        this.place_of_birth = place_of_birth;
    }

    public double getPopularity() {
        return this.popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getİd() {
        return this.id;
    }

    public void setİd(int id) {
        this.id = id;
    }

    public boolean getAdult() {
        return this.adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public Object getHomepage() {
        return this.homepage;
    }

    public void setHomepage(Object homepage) {
        this.homepage = homepage;
    }
}
