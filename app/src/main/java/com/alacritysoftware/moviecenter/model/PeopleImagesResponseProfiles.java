package com.alacritysoftware.moviecenter.model;

public class PeopleImagesResponseProfiles {
    private double aspect_ratio;
    private String file_path;
    private double vote_average;
    private int width;
    private Object iso_639_1;
    private int vote_count;
    private int height;

    public double getAspect_ratio() {
        return this.aspect_ratio;
    }

    public void setAspect_ratio(double aspect_ratio) {
        this.aspect_ratio = aspect_ratio;
    }

    public String getFile_path() {
        return this.file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public double getVote_average() {
        return this.vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Object getİso_639_1() {
        return this.iso_639_1;
    }

    public void setİso_639_1(Object iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public int getVote_count() {
        return this.vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
