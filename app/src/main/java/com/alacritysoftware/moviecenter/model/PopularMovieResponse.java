package com.alacritysoftware.moviecenter.model;

import java.util.List;

public class PopularMovieResponse {
    private int page;
    private int total_pages;
    private List<PopularMovieResponseResults> results;
    private int total_results;

    public List<PopularMovieResponseResults> getResults() {
        return results;
    }

    public void setResults(List<PopularMovieResponseResults> results) {
        this.results = results;
    }



    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_pages() {
        return this.total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }



    public int getTotal_results() {
        return this.total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }
}
