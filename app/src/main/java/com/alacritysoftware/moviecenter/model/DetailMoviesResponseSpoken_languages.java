package com.alacritysoftware.moviecenter.model;

public class DetailMoviesResponseSpoken_languages {
    private String name;
    private String iso_639_1;
    private String english_name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getİso_639_1() {
        return this.iso_639_1;
    }

    public void setİso_639_1(String iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public String getEnglish_name() {
        return this.english_name;
    }

    public void setEnglish_name(String english_name) {
        this.english_name = english_name;
    }
}
