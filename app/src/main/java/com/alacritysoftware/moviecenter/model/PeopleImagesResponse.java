package com.alacritysoftware.moviecenter.model;

import java.util.List;

public class PeopleImagesResponse {
    private List<PeopleImagesResponseProfiles> profiles;
    private int id;

    public List<PeopleImagesResponseProfiles> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<PeopleImagesResponseProfiles> profiles) {
        this.profiles = profiles;
    }

    public int getİd() {
        return this.id;
    }

    public void setİd(int id) {
        this.id = id;
    }
}
