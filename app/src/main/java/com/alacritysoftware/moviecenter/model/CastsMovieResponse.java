package com.alacritysoftware.moviecenter.model;

import java.util.List;

public class CastsMovieResponse {
    private int id;
    private CastsMovieResponseCrew[] crew;
    private List<CastsMovieResponseCast> cast;

    public List<CastsMovieResponseCast> getCastsMovieResponseCasts() {
        return cast;
    }

    public void setCastsMovieResponseCasts(List<CastsMovieResponseCast> cast) {
        this.cast = cast;
    }



    public int getİd() {
        return this.id;
    }

    public void setİd(int id) {
        this.id = id;
    }

    public CastsMovieResponseCrew[] getCrew() {
        return this.crew;
    }

    public void setCrew(CastsMovieResponseCrew[] crew) {
        this.crew = crew;
    }
}
