package com.alacritysoftware.moviecenter.model;

public class PersonPopularResponseResults {
    private int gender;
    private String known_for_department;
    private PersonPopularResponseResultsKnown_for[] known_for;
    private double popularity;
    private String name;
    private String profile_path;
    private int id;
    private boolean adult;

    public int getGender() {
        return this.gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getKnown_for_department() {
        return this.known_for_department;
    }

    public void setKnown_for_department(String known_for_department) {
        this.known_for_department = known_for_department;
    }

    public PersonPopularResponseResultsKnown_for[] getKnown_for() {
        return this.known_for;
    }

    public void setKnown_for(PersonPopularResponseResultsKnown_for[] known_for) {
        this.known_for = known_for;
    }

    public double getPopularity() {
        return this.popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path() {
        return this.profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public int getİd() {
        return this.id;
    }

    public void setİd(int id) {
        this.id = id;
    }

    public boolean getAdult() {
        return this.adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }
}
