package com.alacritysoftware.moviecenter.model;

public class DetailMoviesResponseProduction_countries {
    private String iso_3166_1;
    private String name;

    public String getİso_3166_1() {
        return this.iso_3166_1;
    }

    public void setİso_3166_1(String iso_3166_1) {
        this.iso_3166_1 = iso_3166_1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
