package com.alacritysoftware.moviecenter.model;

public class DetailMoviesResponseGenres {
    private String name;
    private int id;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getİd() {
        return this.id;
    }

    public void setİd(int id) {
        this.id = id;
    }
}
